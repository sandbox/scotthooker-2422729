<?php

/**
 * @file
 * Code for the Commerce Sanitize drush command.
 */

// Allows drush to use db_table_exists
include_once DRUPAL_ROOT . '/includes/database/database.inc';

/**
 * Implements hook_drush_sql_sync_sanitize()
 */
function commerce_sanitize_drush_sql_sync_sanitize($source) {
  if(function_exists('db_table_exists')) {
    if(db_table_exists('commerce_order')) {
      $sanitize_order_emails = "UPDATE commerce_order SET mail = concat('order+', order_id, '@localhost');";
      drush_sql_register_post_sync_op('commerce_sanitize_order_emails', dt('Reset Commerce Order email addresses in commerce_order table'), $sanitize_order_emails);
    }

    if(db_table_exists('commerce_order_revision')) {
      $sanitize_order_revision_emails = "UPDATE commerce_order_revision SET mail = concat('order+', order_id, '@localhost');";
      drush_sql_register_post_sync_op('commerce_sanitize_order_revision_emails', dt('Reset Commerce Order Revision email addresses in commerce_order_revision table'), $sanitize_order_revision_emails);
    }

    if(db_table_exists('commerce_payment_transaction')) {
      $sanitize_payment_payload = "UPDATE commerce_payment_transaction SET payload = '';";
      drush_sql_register_post_sync_op('commerce_sanitize_payment_payload', dt('Empty Commerce Transaction payload column in commerce_payment_transaction table'), $sanitize_payment_payload);
    }

    if(db_table_exists('commerce_paypal_ipn')) {
      $sanitize_paypal_emails = "UPDATE commerce_paypal_ipn SET payer_email = concat('order+', order_id, '@localhost');";
      drush_sql_register_post_sync_op('commerce_sanitize_paypal_payer_emails', dt('Reset Commerce PayPal email addresses in commerce_paypal_ipn table'), $sanitize_paypal_emails);
    }
  }
}
